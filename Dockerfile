ARG NODE_BASE_IMAGE_VERSION=10.21.0-stretch-slim

FROM node:${NODE_BASE_IMAGE_VERSION}

ARG NODE_HOME_DIR=/home/node

RUN mkdir -p ${NODE_HOME_DIR}
COPY . ${NODE_HOME_DIR}

RUN apt-get update
RUN apt-get install -y git procps vim htop

WORKDIR ${NODE_HOME_DIR}
RUN yarn
RUN yarn build

CMD ["bash", "-c", "node", "dist/."]