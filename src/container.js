export const calculateFoodlyPointsEarnedSubTotal = ({subTotal}) => {
  // Points per dollar == 10
  return Math.floor(Math.max(subTotal, 0)) * 10
}
