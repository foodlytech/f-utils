# f-utils

Collection of pure utility functions.

This library is shared between client and server.

Be very careful when adding dependencies as it should be platform independent.

## Building the Image and running the container

- **Docker**

1. `docker build . [-t image_name:image_tag][--name container_name]`
2. ``docker run -it -v `pwd`:/home/node/ image_name:image_tag [bash|node .]``

- **Docker Compose**

1. `docker-compose up -d`
2. `docker-compose exec f-utils bash`

## Stop and remove the Image and container

- **Docker**

1. `docker stop [container_name]`
2. `docker rmi [-t image_name:image_tag]`

- **Docker Compose**

1. docker-compose down --rmi all
